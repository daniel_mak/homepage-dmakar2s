//a2.1
function queue() {
    var queue = {};
    queue.elements = new Array();
    queue.size = 0;
    queue.insert = function(arg) {
        this.size++;
        this.elements.push(arg);
    }
    queue.length = function() {
        return this.size;
    }
    queue.remove = function() {
        return this.elements.splice(this.size-1, 1);
    }
    queue.first = function() {
        return this.elements[0];
    }
    return queue;
}

//a2.2
function unOrdSet() {
    var unOrdSet = {};
    unOrdSet.elements = new Array();
    unOrdSet.size = 0;
    unOrdSet.insert = function(arg) {
        if(this.elements.indexOf(arg) == -1) {
            this.size++;
            this.elements.push(arg);
        } else {
            console.log("Schon vorhanden");
        }
    }
    unOrdSet.remove = function(arg) {
        var index = this.elements.indexOf(arg);
        if(index != -1) {
            this.size--;
            return this.elements.splice(index, 1);
        } else {
            console.log("Nicht vorhanden");
        }
    }
    unOrdSet.length = function() {
        return this.size;
    }
    return unOrdSet;
}

//a2.3
function UnOrdMultSet() {
    var multset = {};
    multset.elements = new Array();
    multset.size = 0;
    multset.insert = function(arg) {
        this.elements.push(arg);
    }
    multset.remove = function(arg) {
        var index = this.elements.indexOf(arg);
        if(index != -1) {
            this.size--;
            return this.elements.splice(index, 1);
        } else {
            console.log("Nicht vorhanden");
        }
    }
    multset.length = function() {
        return this.size;
    }
    return multset;
}