//a1.1
function identity_function(arg) {
    return function() {
        return arg;
    }
}

//a1.2
function addf(x) {
    return function(y) {
        return x + y;
    }
}

//a1.3
function applyf(func) {
    return function(arg1) {
        return function(arg2) {
            return func(arg1,arg2);
        }
    }
}

//a1.4
function curry(func, arg1) {
    return function(arg2) {
        return func(arg1, arg2);
    }
}

//a1.5
function inc(x) {
    return addf(x)(1);
}

//a1.6
function methodize(func) {
    return function(y) {
        return func(this,y);
    }
}

//a1.7
function demethodize(method) {
    return function(arg1, arg2) {
        return method.call(arg1, arg2);
    }
}

//a1.8
function twice(func) {
    return function(arg) {
        return func(arg,arg);
    }
}

//a1.9
function composeu(func1, func2) {
    return function(arg) {
        return func2(func1(arg));
    }
}

//a1.10
function composeb(func1, func2) {
    return function(arg1, arg2, arg3) {
        return func2(arg3, func1(arg1, arg2));
    }
}

//a1.11
function once(func) {
    var test = true;
    if(test) {
        test = false;
        return function(arg1, arg2) {
            func(arg1, arg2);
        }
    }
    console.log("Fehler");
}

//a1.12
function counterf(arg) {
    var counter = {};
    counter.value = arg;
    animal.inc = function() {
        this.value++;
        console.log(this.value);
    }
    animal.dec = function() {
        this.value--;
        console.log(this.value);
    }
    return counter;
}

//a1.13
function revocable(func) {
    var test = true;
    function invoke(arg1) {
        if(test) {
            return func(arg1);
        }
    }
    function revoke() {
        test = false;
    }
}

//a1.14
function arrayWrapper(arr) {
    var arr = arr;
    arr.elements = new Array();
    arr.size = 0;
    arr.get = function(x) {
        return arr[x];
    }
    arr.store = function(arg) {
        arr.push(arg);
    }
    arr.append = function (arg) {
        arr.push(arg);
    }
}