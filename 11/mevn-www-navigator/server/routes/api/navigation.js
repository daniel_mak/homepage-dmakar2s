const express = require('express');
const mongodb = require('mongodb');

const router = express.Router();

// Get Posts
router.get('/', async (req, res) => {
  const nav = await loadNavCollection();
  res.send(await nav.find({}).toArray());
});

// Add Post

router.post('/', async (req, res) => {
  const nav = await loadNavCollection();
  await nav.update(
    { "_id": mongodb.ObjectId(req.body._id) },
    {
      "$push": {
        "content.sideNav": {
          "id": req.body.id,
          "link": req.body.link,
          "declaration": req.body.declaration,
          "content": req.body.content
        }
      }
    });
  res.status(201).send();
});

// Delete Post
/*
router.delete('/:id', async (req, res) => {
  const nav = await loadNavCollection();
  await nav.deleteOne({ _id: new mongodb.ObjectID(req.params.id) });
  res.status(200).send({});
});*/

async function loadNavCollection() {
  const client = await mongodb.MongoClient.connect(
    'mongodb+srv://admin:admin190@vueexpress-4gyye.mongodb.net/vueexpress?retryWrites=true&w=majority',
    {
      useNewUrlParser: true
    }
  );

  return client.db('vue_express').collection('www-navigator');
}

module.exports = router;
