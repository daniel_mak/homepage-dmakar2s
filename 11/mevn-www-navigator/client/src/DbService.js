import axios from 'axios';

const url = 'http://localhost:5000/api/navigation/';

class DbService {
    // get Content
    // "eslint:recommended"
    static getContent() {
        return new Promise(async (resolve, reject) => {
            try {
                const res = await axios.get(url);
                const data = res.data;
                /**resolve(
                    data.map(content => ({
                        ...content,
                    }))
                );**/
                resolve(data);
            } catch(err) {
                reject(err);
            }
        });
    }
    
    // set Content
    static createContent(_id, id, link, declaration, content) {
        return axios.post(url, {
            _id,
            id,
            link,
            declaration,
            content
        })
    }
}

var test = [{"_id":"5eee08805c897567fa649f10","content":{"id":1,"main":{"link":"html","declaration":"HTML","content":"HTML (HyperText Markup Language) is a descriptive language that specifies webpage structure. In 1990, as part of his vision of the Web, Tim Berners-Lee defined the concept of hypertext, which Berners-Lee formalized the following year through a markup mainly based on SGML. The IETF began formally specifying HTML in 1993, and after several drafts released version 2.0 in 1995. In 1994 Berners-Lee founded the W3C to develop the Web. In 1996, the W3C took over the HTML work and published the HTML 3.2 recommendation a year later. HTML 4.0 was released in 1999 and became an ISO standard in 2000. At that time, the W3C nearly abandoned HTML in favor of XHTML, prompting the founding of an independent group called WHATWG in 2004. Thanks to WHATWG, work on HTML5 continued: the two organizations released the first draft in 2008 and the final standard in 2014."},"sideNav":[{"id":1,"link":"IETF","declaration":"IETF","content":"The Internet Engineering Task Force (IETF) is a worldwide organization that drafts specifications governing the mechanisms behind the Internet, especially the TCP/IP or Internet Protocol Suite. The IETF is open, run by volunteers, and sponsored by the Internet Society."},{"id":2,"link":"W3C","declaration":"W3C","content":"The World Wide Web Consortium (W3C) is an international body that maintains Web-related rules and frameworks. It consists of over 350 member-organizations that jointly develop Web standards, run outreach programs, and maintain an open forum for talking about the Web. The W3C coordinates companies in the industry to make sure they implement the same W3C standards. Each standard passes through four stages of maturity: Working Draft (WD), Candidate Recommendation (CR), Proposed Recommendation (PR), and W3C Recommendation (REC)."},{"id":3,"link":"SGML","declaration":"SGML","content":"The Standard Generalized Markup Language (SGML) is an ISO specification for defining declarative markup languages. On the web, HTML 4, XHTML, and XML are popular SGML-based languages. It is worth noting that since its fifth edition, HTML is no longer SGML-based and has its own parsing rules."}]}},{"_id":"5eee08bb5c897567fa649f11","content":{"id":2,"main":{"link":"css","declaration":"CSS","content":"CSS (Cascading Style Sheets) is a declarative language that controls how webpages look in the browser. The browser applies CSS style declarations to selected elements to display them properly. A style declaration contains the properties and their values, which determine how a webpage looks. CSS is one of the three core Web technologies, along with HTML and JavaScript. CSS usually styles HTML elements, but can be also used with other markup languages like SVG or XML. A CSS rule is a set of properties associated with a selector. Here is an example that makes every HTML paragraph yellow against a black background: Cascading refers to the rules that govern how selectors are prioritized to change a page's appearance. This is a very important feature, since a complex website can have thousands of CSS rules. The following CSS rules should be described as an example: Flexbox, Grid, Preprocessor"},"sideNav":[{"id":1,"link":"flexbox","declaration":"flexbox","content":"Flexbox is the commonly-used name for the CSS Flexible Box Layout Module, a layout model for displaying items in a single dimension — as a row or as a column. In the specification, Flexbox is described as a layout model for user interface design. The key feature of Flexbox is the fact that items in a flex layout can grow and shrink. Space can be assigned to the items themselves, or distributed between or around the items. Flexbox also enables alignment of items on the main or cross axis, thus providing a high level of control over the size and alignment of a group of items."},{"id":2,"link":"grid","declaration":"grid","content":"A CSS grid is defined using the grid value of the display property; you can define columns and rows on your grid using the grid-template-rows and grid-template-columns properties. The grid that you define using these properties is described as the explicit grid. If you place content outside of this explicit grid, or if you are relying on auto-placement and the grid algorithm needs to create additional row or column tracks to hold grid items, then extra tracks will be created in the implicit grid. The implicit grid is the grid created automatically due to content being added outside of the tracks defined. In the example below I have created an explicit grid of three columns and two rows. The third row on the grid is an implicit grid row track, formed due to their being more than the six items which fill the explicit tracks."},{"id":3,"link":"preprocessor","declaration":"preprocessor","content":"A CSS preprocessor is a program that lets you generate CSS from the preprocessor's own unique syntax. There are many CSS preprocessors to choose from, however most CSS preprocessors will add some features that don't exist in pure CSS, such as mixin, nesting selector, inheritance selector, and so on. These features make the CSS structure more readable and easier to maintain. To use a CSS preprocessor, you must install a CSS compiler on your web server."}]}}]


export default DbService;