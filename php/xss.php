<!DOCTYPE html>
<html lang="de">

<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="../css/a12.css" rel="stylesheet" type="text/css">
</head>

<body>
<form method="get">
    <fieldset>
        <legend>Login to your Account:</legend>
        Account name:<br>
        <input type="text" name="account">
        <br>
        Password:<br>
        <input type="password" name="password">
        <br><br>
        <input type="submit" value="Submit">
    </fieldset>
        <?php
        //anfällig für <script>...</script>-Eingaben
        if (isset($_GET['account']) && isset($_GET['password'])) {
            $nutzername = $_GET['account'];
            $passwort = $_GET['password'];

            echo $nutzername, ' der Login war erfolgreich';
        }
        //nicht so anfällig
        if (isset($_POST['account']) && isset($_POST['password'])) {
            $nutzername = $_POST['account'];
            $passwort = $_POST['password'];

            if (strpos($nutzername, '<script>') || strpos($passwort, '<script>')) {
                echo 'Sie können hier nicht hacken!';
            } else {
                echo $nutzername, ' der Login war erfolgreich';
            }
        }
        ?>
    </form>
</body>

</html>