<!doctype html>
<h1>Login</h1>
<form method="post">
    <fieldset>
        <legend>Loggin to your Account:</legend>
        Account name:<br>
        <input type="text" name="account">
        <br>
        Password:<br>
        <input type="password" name="password">
        <br><br>
        <input type="submit" value="Submit">
    </fieldset>
</form>
<a href="./register.php">Registrieren</a>

<?PHP
if (isset($_POST['account']) && $_POST['account'] != ""  && isset($_POST['password']) && $_POST['password'] != "") {
    $account = hash("sha384", $_POST['account']);
    $passwd = hash("sha384", $_POST['password']);
    $concat = $account . "," . $passwd;

    $loggedIn = false;

    if (($handle = fopen("passwd.csv", "r")) !== FALSE) {
        while (($data = fgetcsv($handle, 1000, "\n")) !== FALSE) {
            if($data[0] == $concat) {
                $loggedIn = true;
            break;
            }
        }
        fclose($handle);
        if($loggedIn == true) {
            session_start();
            $_SESSION["account"] = $account;
            $_SESSION["passwd"] = $passwd;
            $_SESSION["loggedIn"] = $loggedIn;
            echo "<script>alert('Logged in successfully!')</script>";
            echo '<script type="text/javascript">window.location.href="home.php";</script>';
            die();
        } else {
            echo "<script>alert('Username or Password are wrong!')</script>";
            $_SESSION = array();
        }
    }
}
?>