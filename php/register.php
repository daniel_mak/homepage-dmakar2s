<!doctype html>
<h1>Register</h1>
<form method="post">
    <fieldset>
        <legend>Register a new Account:</legend>
        Account name:<br>
        <input type="text" name="account">
        <br>
        Password:<br>
        <input type="password" name="password">
        <br><br>
        <input type="submit" value="Submit">
    </fieldset>
</form>

<?PHP
if (isset($_POST['account']) && isset($_POST['password'])) {
    $account = hash("sha384", $_POST['account']);
    $passwd = hash("sha384", $_POST['password']);
    $file = './passwd.csv';
    $new_line = $account . ',' . $passwd . "\n";
    if (file_put_contents($file, $new_line, FILE_APPEND | LOCK_EX)) {
        echo "<script>alert('Registered successfully!')</script>";
    }
    echo '<script type="text/javascript">window.location.href="login.php";</script>';
    die();
}
?>