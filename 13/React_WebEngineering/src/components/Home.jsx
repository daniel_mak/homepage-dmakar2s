import React from "react";
import { HEROES } from "../data/heros";
import pic from "../data/superhero.png";

export const Home = () => {
  const heroes = HEROES.map(val => {
    return (
      <div>
        <img src={pic} alt="pic" width="75" />
        <span>{val.name}</span>
        <p>{val.description}</p>
        <hr />
      </div>
    );
  });
  return <ul>{heroes}</ul>;
};
