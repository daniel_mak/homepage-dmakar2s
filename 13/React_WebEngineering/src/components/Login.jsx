import React from "react";
import { HEROES } from "../data/heros";
import { useHistory } from "react-router-dom";
import pic from "../data/react-heros.png";

export const Login = () => {
  const history = useHistory();

  const handleSubmit = event => {
    event.preventDefault();
    if (!event.target.checkValidity()) {
      // form is invalid! so we do nothing
      alert("Login invalid");
      return;
    }
    const data = new FormData(event.target);
    for (var i = 0; i < HEROES.length; i++) {
      if (data.get("username") === HEROES[i].name) {
        history.push("/home");
        return;
      }
    }
    alert("Login invalid");
  };

  return (
    <div class="container">
      <div class="card">
        <h2>Login</h2>
        <img src={pic} alt="pic" width="250" />
        <form onSubmit={handleSubmit} noValidate>
          <input
            id="username"
            name="username"
            type="text"
            required
            placeholder="Username"
          />

          <input
            id="password"
            name="password"
            type="password"
            required
            pattern="^(?=.*[*.!@$%^&(){}[\]:;<>,\.\?\/~_+-=\|]).{6,}"
            placeholder="Password"
          />

          <button>Login!</button>
        </form>
      </div>
    </div>
  );
};
